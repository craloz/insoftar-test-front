import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { ResponseBody } from "src/app/shared/model/response-body.model";
import { User } from "../user.model";
import { UsersService } from "../users.service";
import { map } from 'rxjs/operators';
import { Like } from "src/app/shared/model/like.model";


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['user-list.component.css']
})
export class UserListComponent implements OnInit {

  public usersBody$: Observable<User[]>

  constructor(
    private router: Router, 
    private route: ActivatedRoute,
    private userService: UsersService) {

  }

  public onEditUser(userId: number): void {
    this.router.navigate([`edit/${userId}`], {relativeTo: this.route})
  }

  public onCreateUser(): void {
    this.router.navigate([`create`], {relativeTo: this.route})
  }

  ngOnInit() {
    this.fetchUserList();
  }

  public fetchUserList(): void {
    this.usersBody$ = this.userService.fetchUserList().pipe(map(el => el.body));
  }

  public onAddLike(user: User) {
    const like = new Like();
    like.name = ""
    user.likes.push(like)
  }

  public onModifyLike(likeIndex: number, user: User) {
    const like = user.likes[likeIndex];
    if (like.id) {
      this.userService.editLike(like).subscribe((data) => {
        user.likes[likeIndex] = data.body
        user.likes[likeIndex].editMode = false;
        alert('Gusto modificado satisfactoriamente');
      }, (error) => {
        alert('Error modificando el gusto')
      });
    } else {
      this.userService.createLike(like, user).subscribe((data) => {
        user.likes[likeIndex] = data.body
        user.likes[likeIndex].editMode = false;
        alert('Gusto creado satisfactoriamente');
      }, (error) => {
        alert('Error creando el gusto')
      })
    }
  }

}