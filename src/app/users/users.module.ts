import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { UserListComponent } from "./user-list/user-list.component";
import { UserModifyComponent } from "./user-modify/user-modify.component";
import { UsersRoutingModule } from "./users-routing.module";
import { UsersComponent } from "./users.component";
import { UsersService } from "./users.service";

@NgModule({
  declarations: [
    UsersComponent, 
    UserListComponent, 
    UserModifyComponent,
    
  ],
  providers: [
    UsersService
  ],
  exports: [],
  imports: [
    CommonModule,
    RouterModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
})
export class UsersModule {

}