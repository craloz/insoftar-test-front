import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { map } from "rxjs/operators";
import { User } from "../user.model";
import { UsersService } from "../users.service";

@Component({
  selector: 'app-user-modify',
  templateUrl: './user-modify.component.html',
  styleUrls: ['./user-modify.component.css']
})
export class UserModifyComponent implements OnInit {

  public userForm: FormGroup;
  public userId: string;
  public user: User;

  constructor(
    private router: Router, 
    private route: ActivatedRoute,
    private userService: UsersService) {

  }

  public onCancel(): void {
    this.router.navigate(['../'])
  }

  ngOnInit() {
    
    this.userId = this.route.snapshot.params.userId;
    this.user = new User();

    if (this.userId) {
      this.userService.fetchUser(this.userId).pipe(map(data => data.body)).subscribe(
      (data) => {
        this.user = data;
        this.userForm.get('firstName').patchValue(this.user.firstName)
        this.userForm.get('lastName').patchValue(this.user.lastName)
        this.userForm.get('idNumber').patchValue(this.user.idNumber)
        this.userForm.get('email').patchValue(this.user.email)
        this.userForm.get('telephone').patchValue(this.user.telephone)
      }) 
    } else {
    }

    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, Validators.required),
      lastName: new FormControl(this.user.lastName, []),
      idNumber: new FormControl(this.user.idNumber, [Validators.required, Validators.pattern('^[0-9]*$')]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email]),
      telephone: new FormControl(this.user.telephone, [Validators.required, , Validators.pattern('^[0-9]*$')]),
    })
  }

  onSubmit(): void {
    this.onModifyUser();
  }

  public onModifyUser(): void {
    this.user.firstName = this.userForm.get('firstName').value;
    this.user.lastName = this.userForm.get('lastName').value;
    this.user.idNumber = this.userForm.get('idNumber').value;
    this.user.email = this.userForm.get('email').value;
    this.user.telephone = this.userForm.get('telephone').value;
    this.userId ? this.editUser() : this.createUser();
  }

  public createUser(): void {
    
    this.userService.createUser(this.user).subscribe((data) => {
      alert('Usuario creado satisfactoriamente');
      this.router.navigate(['../'])
    }, (error) => {
      this.showErrors(error.error.code)
    })
  }

  public editUser(): void {
    this.userService.editUser(this.user).subscribe((data) => {
      alert('Usuario modificado satisfactoriamente');
      this.router.navigate(['../']);
    }, (error) => {      
      this.showErrors(error.error.code);
    })
  }

  public showErrors(errorCode: string): void {

    let errorMsg = 'Error: '

    switch (errorCode) {
      case "EMAIL_ALREADY_IN_USE":
        alert(`${errorMsg} El correo ya se encuentra en uso`);
        break;
    
      case "ID_NUMBER_ALREADY_IN_USE":
        alert(`${errorMsg} La cedula ya se encuentra en uso`);
        break;
      default: 
        break;
    }
  }
  

}