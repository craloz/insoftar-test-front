import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Like } from "../shared/model/like.model";
import { ResponseBody } from "../shared/model/response-body.model";
import { User } from "./user.model";

@Injectable()
export class UsersService { 

  constructor(private http: HttpClient) {

  }

  public fetchUserList(): Observable<ResponseBody<User[]>> {
    return this.http.get<ResponseBody<User[]>>(`${environment.basePath}/users/list`);
  }

  public fetchUser(id: string): Observable<ResponseBody<User>> {
    return this.http.get<ResponseBody<User>>(`${environment.basePath}/users/user/${id}`);
  }

  public createUser(user: User): Observable<ResponseBody<User>> {
    return this.http.post<ResponseBody<User>>(`${environment.basePath}/users`, user);
  }

  public editUser(user: User): Observable<ResponseBody<User>> {
    return this.http.put<ResponseBody<User>>(`${environment.basePath}/users`, user);
  }

  public createLike(like: Like, user: User): Observable<ResponseBody<Like>>{
    return this.http.post<ResponseBody<Like>>(`${environment.basePath}/users/user/${user.id}/likes`, like);
  }

  public editLike(like: Like): Observable<ResponseBody<Like>>{
    return this.http.put<ResponseBody<Like>>(`${environment.basePath}/users/user/likes`, like);
  }

}