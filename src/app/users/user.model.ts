import { Like } from "../shared/model/like.model";

export class User {

  public id: number;
  public firstName: string;
  public lastName: string;
  public idNumber: number;
  public email: string;
  public telephone: number;
  public showLikes: boolean = false;
  public likes: Array<Like>;
  
}