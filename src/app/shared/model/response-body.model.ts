export class ResponseBody<T> {
  body: T;
  code: string
  type: string
}